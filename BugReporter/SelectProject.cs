﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class SelectProject : Form
    {
        int ProjectID;
        String Name;
        SqlConnection SqlConnection;
        public SelectProject()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SelectProject SelectProject = new SelectProject();
            SelectProject.Dispose();
        }

        public int Project
        {
            get { return ProjectID; }
        }

        public String ProjectName
        {
            get { return Name; }
        }

        private void populateListBox()
        {
            String selcmd = "SELECT PROJECTID, NAME, CREATED_ON, TARGET_END FROM PROJECTS";

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["PROJECTID"].ToString(), SqlDataReader["NAME"].ToString(), SqlDataReader["CREATED_ON"].ToString(), SqlDataReader["TARGET_END"].ToString() };
                    var listViewItem = new ListViewItem(row);
                    listProjects.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void SelectProject_Load(object sender, EventArgs e)
        {
            populateListBox();
        }

        private void listProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listProjects.SelectedItems.Count > 0)
            {
                ListViewItem item = listProjects.SelectedItems[0];
                ProjectID = Int32.Parse(item.SubItems[0].Text);
                Name = item.SubItems[1].Text;
                this.DialogResult = DialogResult.OK;
                SelectProject SelectProject = new SelectProject();
                SelectProject.Dispose();
            }
        }
    }
}
