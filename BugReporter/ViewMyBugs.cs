﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class ViewMyBugs : Form
    {
        public static int OriginalBugID;
        SqlConnection SqlConnection;
        public ViewMyBugs()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30;MultipleActiveResultSets=True;");
            InitializeComponent();
        }

        private void UpdateBug_Load(object sender, EventArgs e)
        {
            populateListBox();
        }

        private void populateListBox()
        {
            String selcmd = "SELECT * FROM BUG WHERE AUDIT_ID = 1 AND ASSIGNED_TO = " + Variables.userID;

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["ORIGINAL_BUGID"].ToString(), SqlDataReader["SUMMARY"].ToString(), SqlDataReader["DESCRIPTION"].ToString(), SqlDataReader["IDENTIFIED_ON"].ToString(), SqlDataReader["IDENTIFIED_BY"].ToString(), SqlDataReader["PRIORITY"].ToString(), SqlDataReader["METHOD"].ToString(), SqlDataReader["CLASS"].ToString(), SqlDataReader["SOURCEFILE"].ToString() };
                    var listViewItem = new ListViewItem(row);

                    String selcmd2 = "SELECT * FROM BUG";

                    SqlCommand mySqlCommand2 = new SqlCommand(selcmd2, SqlConnection);

                    try
                    {

                        SqlDataReader SqlDataReader2 = mySqlCommand2.ExecuteReader();

                        while (SqlDataReader2.Read())
                        {
                            if (SqlDataReader2["PROGRESS"].ToString() == "COMPLETED" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Green;
                            }
                            else if (SqlDataReader2["PROGRESS"].ToString() == "IN PROGRESS" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Orange;
                            }
                            else if (SqlDataReader2["PROGRESS"].ToString() == "NOT STARTED" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Red;
                            }
                        }
                    }

                    catch (SqlException ex)
                    {

                        MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    listBugs.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void listBugs_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBugs.SelectedItems.Count > 0)
            {
                ListViewItem item = listBugs.SelectedItems[0];
                OriginalBugID = Int32.Parse(item.SubItems[0].Text);
            }
        }

        public static int _OriginalBugID
        {
            get
            {
                return OriginalBugID;
            }
        }

        private void btnUpdateBug_Click(object sender, EventArgs e)
        {
            UpdateBug UpdateBug = new UpdateBug();
            UpdateBug.Show();
        }

        private void ViewMyBugs_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            BugMenu BugMenu = new BugMenu();
            BugMenu.Show();
        }
    }
}
