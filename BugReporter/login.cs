﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{

    public partial class login : Form
    {
        SqlConnection SqlConnection;
        public login()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");
        }

        private void registerButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            register reg = new register();
            reg.Show();
        }

        private void login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "SELECT * FROM USERS WHERE USERNAME=@USERNAME COLLATE SQL_Latin1_General_CP1_CS_AS and PASSWORD = @PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS";

                loginTry(usernameTxt.Text, passwordTxt.Text, commandString);


            }
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(usernameTxt.Text) ||
                string.IsNullOrEmpty(passwordTxt.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool loginTry(String username, String password, String commandString)
        {

            try
            {

                SqlConnection.Open();

                SqlCommand cmd = new SqlCommand(commandString, SqlConnection);

                cmd.Parameters.AddWithValue("@USERNAME", username);
                cmd.Parameters.AddWithValue("@PASSWORD", password);
                cmd.ExecuteNonQuery();

                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                SqlDataReader sqlDataReader = cmd.ExecuteReader();

                
                int count = ds.Tables[0].Rows.Count;
                //If count is equal to 1, than show frmMain form
                if (count == 1)
                {
                    while (sqlDataReader.Read())
                    {
                        Variables.userID = (int)sqlDataReader["USERID"];
                        Variables.userName = sqlDataReader["USERNAME"].ToString();
                    }


                    MessageBox.Show("Login Successful!");
                    this.Hide();
                    BugMenu BugMenu = new BugMenu();
                    BugMenu.Show();
                    return true;
                }
                else
                {
                    MessageBox.Show("Login Failed!");
                    return false;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            SqlConnection.Close();

        }

        private void login_Load(object sender, EventArgs e)
        {

        }
    }
}
 