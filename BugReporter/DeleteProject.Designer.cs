﻿namespace BugReporter
{
    partial class DeleteProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnProjectID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCreatedby = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnStart = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTargetend = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCreatedon = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnDeleteProject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnProjectID,
            this.columnName,
            this.columnCreatedby,
            this.columnStart,
            this.columnTargetend,
            this.columnCreatedon});
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(141, 40);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(568, 370);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnProjectID
            // 
            this.columnProjectID.Width = 0;
            // 
            // columnName
            // 
            this.columnName.Tag = "";
            this.columnName.Text = "Name";
            this.columnName.Width = 100;
            // 
            // columnCreatedby
            // 
            this.columnCreatedby.Text = "Created By";
            this.columnCreatedby.Width = 100;
            // 
            // columnStart
            // 
            this.columnStart.Text = "Start";
            this.columnStart.Width = 120;
            // 
            // columnTargetend
            // 
            this.columnTargetend.Text = "Target End";
            this.columnTargetend.Width = 120;
            // 
            // columnCreatedon
            // 
            this.columnCreatedon.Text = "Created On";
            this.columnCreatedon.Width = 120;
            // 
            // btnDeleteProject
            // 
            this.btnDeleteProject.Location = new System.Drawing.Point(394, 454);
            this.btnDeleteProject.Name = "btnDeleteProject";
            this.btnDeleteProject.Size = new System.Drawing.Size(104, 36);
            this.btnDeleteProject.TabIndex = 3;
            this.btnDeleteProject.Text = "Delete";
            this.btnDeleteProject.UseVisualStyleBackColor = true;
            this.btnDeleteProject.Click += new System.EventHandler(this.btnDeleteProject_Click);
            // 
            // DeleteProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 545);
            this.Controls.Add(this.btnDeleteProject);
            this.Controls.Add(this.listView1);
            this.Name = "DeleteProject";
            this.Text = "DeleteProject";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DeleteProject_FormClosed);
            this.Load += new System.EventHandler(this.DeleteProject_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnProjectID;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnCreatedby;
        private System.Windows.Forms.ColumnHeader columnStart;
        private System.Windows.Forms.ColumnHeader columnTargetend;
        private System.Windows.Forms.ColumnHeader columnCreatedon;
        private System.Windows.Forms.Button btnDeleteProject;
    }
}