﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class SelectUser : Form
    {
        int UserID;
        String Name;
        SqlConnection SqlConnection;
        public SelectUser()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30");
        }

        public int User
        {
            get
            {
                return UserID;
            }
        }

        public String userName
        {
            get
            {
                return Name;
            }
        }

        private void populateListBox()
        {
            String selcmd = "SELECT USERID, NAME, ROLE FROM USERS ORDER BY ROLE";

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["USERID"].ToString(), SqlDataReader["NAME"].ToString(), SqlDataReader["ROLE"].ToString() };
                    var listViewItem = new ListViewItem(row);
                    listUsers.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void SelectUser_Load(object sender, EventArgs e)
        {
            populateListBox();
        }

        private void listUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listUsers.SelectedItems.Count > 0)
            {
                ListViewItem item = listUsers.SelectedItems[0];
                UserID = Int32.Parse(item.SubItems[0].Text);
                Name = item.SubItems[1].Text + " " + item.SubItems[2].Text;
                this.DialogResult = DialogResult.OK;
                SelectUser SelectUser = new SelectUser();
                SelectUser.Dispose();
            }
        }
    }
}
