﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class register : Form
    {
        SqlConnection SqlConnection;
        public register()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");

        }

        private void register_FormClosed(object sender, FormClosedEventArgs e)
        {
            login login = new login();
            login.Show();
        }

        private void registerButton_Click(object sender, EventArgs e)
        {

            if (checkInputs())
            {

                String commandString = "INSERT INTO USERS(NAME, EMAIL, USERNAME, PASSWORD, ROLE) VALUES (@name, @email, @username, @password, @role)";

                insertRecord(nameTxt.Text, emailTxt.Text, usernameTxt.Text, passwordTxt.Text, roleCombo.Text, commandString);
            }

        }
        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(nameTxt.Text) ||
                string.IsNullOrEmpty(emailTxt.Text) ||
                string.IsNullOrEmpty(passwordTxt.Text) ||
                string.IsNullOrEmpty(roleCombo.Text) ||
                string.IsNullOrEmpty(usernameTxt.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool insertRecord(String name, String email, String username, String password, String role, String commandString)
        {

            try
            {

                SqlConnection.Open();

                SqlCommand cmdInsert = new SqlCommand(commandString, SqlConnection);

                cmdInsert.Parameters.AddWithValue("@NAME", name);
                cmdInsert.Parameters.AddWithValue("@EMAIL", email);
                cmdInsert.Parameters.AddWithValue("@USERNAME", username);
                cmdInsert.Parameters.AddWithValue("@PASSWORD", password);
                cmdInsert.Parameters.AddWithValue("@ROLE", role);
                cmdInsert.ExecuteNonQuery();
                return true;
                
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            SqlConnection.Close();
            

        }

        private void register_Load(object sender, EventArgs e)
        {

        }
    }
}
