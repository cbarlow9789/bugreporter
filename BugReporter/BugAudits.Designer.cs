﻿namespace BugReporter
{
    partial class BugAudits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBugVersion = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblSourceFile = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.lblMethod = new System.Windows.Forms.Label();
            this.txtCode = new ICSharpCode.TextEditor.TextEditorControl();
            this.lblPriority = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblCreatedBy = new System.Windows.Forms.Label();
            this.lblResolutionDate = new System.Windows.Forms.Label();
            this.lblProgress = new System.Windows.Forms.Label();
            this.lblProject = new System.Windows.Forms.Label();
            this.lblAssignedTo = new System.Windows.Forms.Label();
            this.lblIdentifiedOn = new System.Windows.Forms.Label();
            this.lblIdentifiedBy = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblSummary = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBugVersion
            // 
            this.comboBugVersion.FormattingEnabled = true;
            this.comboBugVersion.Location = new System.Drawing.Point(164, 24);
            this.comboBugVersion.Name = "comboBugVersion";
            this.comboBugVersion.Size = new System.Drawing.Size(396, 28);
            this.comboBugVersion.TabIndex = 0;
            this.comboBugVersion.SelectedIndexChanged += new System.EventHandler(this.comboBugVersion_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Bug Version";
            // 
            // lblSourceFile
            // 
            this.lblSourceFile.AutoSize = true;
            this.lblSourceFile.Location = new System.Drawing.Point(658, 264);
            this.lblSourceFile.Name = "lblSourceFile";
            this.lblSourceFile.Size = new System.Drawing.Size(89, 20);
            this.lblSourceFile.TabIndex = 68;
            this.lblSourceFile.Text = "Source File";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(375, 264);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(48, 20);
            this.lblClass.TabIndex = 67;
            this.lblClass.Text = "Class";
            // 
            // lblMethod
            // 
            this.lblMethod.AutoSize = true;
            this.lblMethod.Location = new System.Drawing.Point(15, 264);
            this.lblMethod.Name = "lblMethod";
            this.lblMethod.Size = new System.Drawing.Size(63, 20);
            this.lblMethod.TabIndex = 66;
            this.lblMethod.Text = "Method";
            // 
            // txtCode
            // 
            this.txtCode.IsReadOnly = false;
            this.txtCode.Location = new System.Drawing.Point(18, 329);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(1500, 460);
            this.txtCode.TabIndex = 65;
            this.txtCode.Load += new System.EventHandler(this.txtCode_Load);
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Location = new System.Drawing.Point(375, 168);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(56, 20);
            this.lblPriority.TabIndex = 63;
            this.lblPriority.Text = "Priority";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 306);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 20);
            this.label10.TabIndex = 62;
            this.label10.Text = "Code";
            // 
            // lblCreatedBy
            // 
            this.lblCreatedBy.AutoSize = true;
            this.lblCreatedBy.Location = new System.Drawing.Point(14, 234);
            this.lblCreatedBy.Name = "lblCreatedBy";
            this.lblCreatedBy.Size = new System.Drawing.Size(88, 20);
            this.lblCreatedBy.TabIndex = 47;
            this.lblCreatedBy.Text = "Created By";
            // 
            // lblResolutionDate
            // 
            this.lblResolutionDate.AutoSize = true;
            this.lblResolutionDate.Location = new System.Drawing.Point(13, 201);
            this.lblResolutionDate.Name = "lblResolutionDate";
            this.lblResolutionDate.Size = new System.Drawing.Size(124, 20);
            this.lblResolutionDate.TabIndex = 46;
            this.lblResolutionDate.Text = "Resolution Date";
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(13, 168);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(72, 20);
            this.lblProgress.TabIndex = 45;
            this.lblProgress.Text = "Progress";
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.Location = new System.Drawing.Point(13, 137);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(58, 20);
            this.lblProject.TabIndex = 44;
            this.lblProject.Text = "Project";
            // 
            // lblAssignedTo
            // 
            this.lblAssignedTo.AutoSize = true;
            this.lblAssignedTo.Location = new System.Drawing.Point(658, 106);
            this.lblAssignedTo.Name = "lblAssignedTo";
            this.lblAssignedTo.Size = new System.Drawing.Size(97, 20);
            this.lblAssignedTo.TabIndex = 43;
            this.lblAssignedTo.Text = "Assigned To";
            // 
            // lblIdentifiedOn
            // 
            this.lblIdentifiedOn.AutoSize = true;
            this.lblIdentifiedOn.Location = new System.Drawing.Point(375, 106);
            this.lblIdentifiedOn.Name = "lblIdentifiedOn";
            this.lblIdentifiedOn.Size = new System.Drawing.Size(100, 20);
            this.lblIdentifiedOn.TabIndex = 42;
            this.lblIdentifiedOn.Text = "Identified On";
            // 
            // lblIdentifiedBy
            // 
            this.lblIdentifiedBy.AutoSize = true;
            this.lblIdentifiedBy.Location = new System.Drawing.Point(13, 106);
            this.lblIdentifiedBy.Name = "lblIdentifiedBy";
            this.lblIdentifiedBy.Size = new System.Drawing.Size(97, 20);
            this.lblIdentifiedBy.TabIndex = 41;
            this.lblIdentifiedBy.Text = "Identified By";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(375, 76);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(89, 20);
            this.lblDescription.TabIndex = 40;
            this.lblDescription.Text = "Description";
            // 
            // lblSummary
            // 
            this.lblSummary.AutoSize = true;
            this.lblSummary.Location = new System.Drawing.Point(13, 76);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(80, 20);
            this.lblSummary.TabIndex = 39;
            this.lblSummary.Text = "Summary:";
            // 
            // BugAudits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 707);
            this.Controls.Add(this.lblSourceFile);
            this.Controls.Add(this.lblClass);
            this.Controls.Add(this.lblMethod);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.lblPriority);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblCreatedBy);
            this.Controls.Add(this.lblResolutionDate);
            this.Controls.Add(this.lblProgress);
            this.Controls.Add(this.lblProject);
            this.Controls.Add(this.lblAssignedTo);
            this.Controls.Add(this.lblIdentifiedOn);
            this.Controls.Add(this.lblIdentifiedBy);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblSummary);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBugVersion);
            this.Name = "BugAudits";
            this.Text = "BugAudits";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BugAudits_FormClosed);
            this.Load += new System.EventHandler(this.BugAudits_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBugVersion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblSourceFile;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Label lblMethod;
        private ICSharpCode.TextEditor.TextEditorControl txtCode;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCreatedBy;
        private System.Windows.Forms.Label lblResolutionDate;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.Label lblProject;
        private System.Windows.Forms.Label lblAssignedTo;
        private System.Windows.Forms.Label lblIdentifiedOn;
        private System.Windows.Forms.Label lblIdentifiedBy;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblSummary;
    }
}