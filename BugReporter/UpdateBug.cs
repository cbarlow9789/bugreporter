﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class UpdateBug : Form
    {
        SqlConnection SqlConnection;
        int OriginalBugID;
        int AuditID;
        String Progress = null;
        int IdentifiedByID;
        int AssignedToID;
        int CreatedByID;
        int ProjectID;
        public UpdateBug()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30;MultipleActiveResultSets=true");
            InitializeComponent();
        }

        private void UpdateBug_Load(object sender, EventArgs e)
        {
            using (ViewMyBugs ViewMyBugs = new ViewMyBugs())
            OriginalBugID = ViewMyBugs._OriginalBugID;
            String selcmd = "SELECT * FROM BUG WHERE ORIGINAL_BUGID = " + OriginalBugID;

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    AuditID = 0;
                    if (Int32.Parse(SqlDataReader["AUDIT_ID"].ToString()) > AuditID)
                    {
                        AuditID = Int32.Parse(SqlDataReader["AUDIT_ID"].ToString());
                        lblSummary.Text = "Summary: " + SqlDataReader["SUMMARY"].ToString();
                        lblDescription.Text = "Description: " + SqlDataReader["Description"].ToString();
                        lblAuditID.Text = "Audit ID: " + SqlDataReader["AUDIT_ID"].ToString();

                        IdentifiedByID = Int32.Parse(SqlDataReader["IDENTIFIED_BY"].ToString());
                        AssignedToID = Int32.Parse(SqlDataReader["ASSIGNED_TO"].ToString());
                        CreatedByID = Int32.Parse(SqlDataReader["CREATED_BY"].ToString());
                        ProjectID = Int32.Parse(SqlDataReader["PROJECTID"].ToString());

                        lblIdentifiedOn.Text = "Identified On: " + SqlDataReader["IDENTIFIED_ON"];
                        txtIdentifiedOn.Text = SqlDataReader["IDENTIFIED_ON"].ToString();

                        lblPriority.Text = "Priority: " + SqlDataReader["PRIORITY"];
                        txtPriority.Text = SqlDataReader["PRIORITY"].ToString();

                        lblProgress.Text = "Progress: " + SqlDataReader["PROGRESS"];
                        lblResolutionDate.Text = "Resolution Date: " + SqlDataReader["RESOLUTION_DATE"];
                        lblMethod.Text = "Method: " + SqlDataReader["METHOD"];
                        lblClass.Text = "Class: " + SqlDataReader["CLASS"];
                        lblSourceFile.Text = "SourceFile: " + SqlDataReader["SOURCEFILE"];
                        txtAuditCode.Text = SqlDataReader["CODE"].ToString();
                        AuditID = AuditID + 1;
                        txtAuditID.Text = AuditID.ToString();

                    }
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SqlConnection.Close();
            GetNames();
        }

        private void GetNames()
        {
            //IDENTIFIED_BY
            String selcmd = "SELECT NAME, ROLE FROM USERS WHERE USERID = " + IdentifiedByID;

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblIdentifiedBy.Text = "Identified By: " + SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                    txtIdentifiedBy.Text = SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            //ASSIGNED_TO
            selcmd = "SELECT NAME, ROLE FROM USERS WHERE USERID = " + AssignedToID;

            mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblAssignedTo.Text = "Assigned to: " + SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                    txtAssignedTo.Text = SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



            //CREATED_BY
            selcmd = "SELECT NAME, ROLE FROM USERS WHERE USERID = " + CreatedByID;

            mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblCreatedBy.Text = "Created By: " + SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                    txtCreatedBy.Text = SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //PROJECT
            selcmd = "SELECT NAME FROM PROJECTS WHERE PROJECTID = " + ProjectID;

            mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblProject.Text = "Project: " + SqlDataReader["NAME"].ToString();
                    txtProject.Text = SqlDataReader["NAME"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void btnUpdateBug_Click(object sender, EventArgs e)
        {
            {
                if (rbProgressNotStarted.Checked)
                {
                    Progress = "NOT STARTED";
                }
                else if (rbProgressInProgress.Checked)
                {
                    Progress = "IN PROGRESS";
                }
                else if (rbProgressCompleted.Checked)
                {
                    Progress = "COMPLETED";
                }

                if (checkInputs())
                {
                    String commandString = "INSERT INTO BUG(SUMMARY, DESCRIPTION, IDENTIFIED_BY, IDENTIFIED_ON, ASSIGNED_TO, PROJECTID, PRIORITY, PROGRESS, RESOLUTION_DATE, CREATED_BY, MODIFIED_BY, AUDIT_ID, CODE, METHOD, CLASS, SOURCEFILE, ORIGINAL_BUGID) VALUES (@SUMMARY, @DESCRIPTION, @IDENTIFIED_BY, @IDENTIFIED_ON, @ASSIGNED_TO, @PROJECTID, @PRIORITY, @PROGRESS, @RESOLUTION_DATE, @CREATED_BY, @MODIFIED_BY, @AUDIT_ID, @CODE, @METHOD, @CLASS, @SOURCEFILE, @ORIGINALBUGID)";
                    insertRecord(txtSummary.Text, txtDescription.Text, IdentifiedByID, DateTime.Parse(txtIdentifiedOn.Text), AssignedToID, ProjectID, txtPriority.Text, Progress, dateResolutionDate.Value, Variables.userID, Variables.userID, AuditID + 1, txtCode.Text, txtMethod.Text, txtClass.Text, txtSourceFile.Text, OriginalBugID, commandString);
                    MessageBox.Show("Success: Bug Successfully Updated");
                    ClearInputs();
                    AuditID = AuditID + 1;
                    txtAuditID.Text = AuditID.ToString();
                }

            }
        }

        public void insertRecord(String Summary, String Description, int IdentifiedBy, DateTime IdentifiedOn, int AssignedToID, int ProjectID, String Priority, String Progress, DateTime ResolutionDate, int CreatedBy, int ModifiedBy, int AuditID, String Code, String Method, String Class, String SourceFile, int OriginalBugID, String commandString)
        {

            try
            {

                SqlConnection.Open();

                SqlCommand cmdInsert = new SqlCommand(commandString, SqlConnection);

                cmdInsert.Parameters.AddWithValue("@SUMMARY", Summary);
                cmdInsert.Parameters.AddWithValue("@DESCRIPTION", Description);
                cmdInsert.Parameters.AddWithValue("@IDENTIFIED_BY", IdentifiedBy);
                cmdInsert.Parameters.AddWithValue("@IDENTIFIED_ON", IdentifiedOn);
                cmdInsert.Parameters.AddWithValue("@ASSIGNED_TO", AssignedToID);
                cmdInsert.Parameters.AddWithValue("@PROJECTID", ProjectID);
                cmdInsert.Parameters.AddWithValue("@PRIORITY", Priority);
                cmdInsert.Parameters.AddWithValue("@PROGRESS", Progress);
                cmdInsert.Parameters.AddWithValue("@RESOLUTION_DATE", ResolutionDate);
                cmdInsert.Parameters.AddWithValue("@CREATED_BY", CreatedBy);
                cmdInsert.Parameters.AddWithValue("@MODIFIED_BY", ModifiedBy);
                cmdInsert.Parameters.AddWithValue("@AUDIT_ID", AuditID);
                cmdInsert.Parameters.AddWithValue("@CODE", Code);
                cmdInsert.Parameters.AddWithValue("@METHOD", Method);
                cmdInsert.Parameters.AddWithValue("@CLASS", Class);
                cmdInsert.Parameters.AddWithValue("@SOURCEFILE", SourceFile);
                cmdInsert.Parameters.AddWithValue("@ORIGINALBUGID", OriginalBugID);

                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SqlConnection.Close();

        }

        private void txtAuditCode_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {
                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtCode.SetHighlighting("C#");
            }
        }

        private void txtCode_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {
                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtCode.SetHighlighting("C#");
            }
        }

        public void ClearInputs()
        {
            txtSummary.Text = "";
            txtDescription.Text = "";
            txtMethod.Text = "";
            txtClass.Text = "";
            txtSourceFile.Text = "";
            rbProgressInProgress.Checked = false;
            rbProgressCompleted.Checked = false;
            rbProgressNotStarted.Checked = false;
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;
            if (string.IsNullOrEmpty(txtSummary.Text))
            {
                MessageBox.Show("Error: Please Enter Summary");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtDescription.Text))
            {
                MessageBox.Show("Error: Please Enter Description");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtMethod.Text))
            {
                MessageBox.Show("Error: Please Enter Method");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtClass.Text))
            {
                MessageBox.Show("Error: Please Enter Class");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtSourceFile.Text))
            {
                MessageBox.Show("Error: Please Enter SourceFile");
                rtnvalue = false;
            }
            if (Progress == null)
            {
                MessageBox.Show("Error: Please Select Progress Value");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

    }
}
