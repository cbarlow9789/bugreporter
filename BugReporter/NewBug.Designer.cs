﻿namespace BugReporter
{
    partial class NewBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupProgress = new System.Windows.Forms.GroupBox();
            this.rbProgressCompleted = new System.Windows.Forms.RadioButton();
            this.rbProgressInProgress = new System.Windows.Forms.RadioButton();
            this.rbProgressNotStarted = new System.Windows.Forms.RadioButton();
            this.lblProject = new System.Windows.Forms.Label();
            this.groupboxPriority = new System.Windows.Forms.GroupBox();
            this.rbPriorityHigh = new System.Windows.Forms.RadioButton();
            this.rbPriorityNormal = new System.Windows.Forms.RadioButton();
            this.rbPriorityLow = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.lblCreatedBy = new System.Windows.Forms.Label();
            this.dateResolutionDate = new System.Windows.Forms.DateTimePicker();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.txtMethod = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCode = new ICSharpCode.TextEditor.TextEditorControl();
            this.label11 = new System.Windows.Forms.Label();
            this.btnSelectProject = new System.Windows.Forms.Button();
            this.lblAssignedTo = new System.Windows.Forms.Label();
            this.btnSelectAssignedTo = new System.Windows.Forms.Button();
            this.btnSaveBug = new System.Windows.Forms.Button();
            this.dateIdentifiedOn = new System.Windows.Forms.DateTimePicker();
            this.lblIdentifiedBy = new System.Windows.Forms.Label();
            this.btnSelectIdentifiedBy = new System.Windows.Forms.Button();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtSummary = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupProgress.SuspendLayout();
            this.groupboxPriority.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupProgress);
            this.groupBox1.Controls.Add(this.lblProject);
            this.groupBox1.Controls.Add(this.groupboxPriority);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblCreatedBy);
            this.groupBox1.Controls.Add(this.dateResolutionDate);
            this.groupBox1.Controls.Add(this.txtSourceFile);
            this.groupBox1.Controls.Add(this.txtClass);
            this.groupBox1.Controls.Add(this.txtMethod);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtCode);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.btnSelectProject);
            this.groupBox1.Controls.Add(this.lblAssignedTo);
            this.groupBox1.Controls.Add(this.btnSelectAssignedTo);
            this.groupBox1.Controls.Add(this.btnSaveBug);
            this.groupBox1.Controls.Add(this.dateIdentifiedOn);
            this.groupBox1.Controls.Add(this.lblIdentifiedBy);
            this.groupBox1.Controls.Add(this.btnSelectIdentifiedBy);
            this.groupBox1.Controls.Add(this.txtDescription);
            this.groupBox1.Controls.Add(this.txtSummary);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1162, 1020);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "New Bug";
            // 
            // groupProgress
            // 
            this.groupProgress.Controls.Add(this.rbProgressCompleted);
            this.groupProgress.Controls.Add(this.rbProgressInProgress);
            this.groupProgress.Controls.Add(this.rbProgressNotStarted);
            this.groupProgress.Location = new System.Drawing.Point(142, 193);
            this.groupProgress.Name = "groupProgress";
            this.groupProgress.Size = new System.Drawing.Size(561, 53);
            this.groupProgress.TabIndex = 58;
            this.groupProgress.TabStop = false;
            this.groupProgress.Text = "Progress";
            // 
            // rbProgressCompleted
            // 
            this.rbProgressCompleted.AutoSize = true;
            this.rbProgressCompleted.Location = new System.Drawing.Point(291, 23);
            this.rbProgressCompleted.Name = "rbProgressCompleted";
            this.rbProgressCompleted.Size = new System.Drawing.Size(111, 24);
            this.rbProgressCompleted.TabIndex = 2;
            this.rbProgressCompleted.TabStop = true;
            this.rbProgressCompleted.Text = "Completed";
            this.rbProgressCompleted.UseVisualStyleBackColor = true;
            // 
            // rbProgressInProgress
            // 
            this.rbProgressInProgress.AutoSize = true;
            this.rbProgressInProgress.Location = new System.Drawing.Point(158, 22);
            this.rbProgressInProgress.Name = "rbProgressInProgress";
            this.rbProgressInProgress.Size = new System.Drawing.Size(115, 24);
            this.rbProgressInProgress.TabIndex = 1;
            this.rbProgressInProgress.Text = "In Progress";
            this.rbProgressInProgress.UseVisualStyleBackColor = true;
            // 
            // rbProgressNotStarted
            // 
            this.rbProgressNotStarted.AutoSize = true;
            this.rbProgressNotStarted.Location = new System.Drawing.Point(25, 23);
            this.rbProgressNotStarted.Name = "rbProgressNotStarted";
            this.rbProgressNotStarted.Size = new System.Drawing.Size(116, 24);
            this.rbProgressNotStarted.TabIndex = 0;
            this.rbProgressNotStarted.TabStop = true;
            this.rbProgressNotStarted.Text = "Not Started";
            this.rbProgressNotStarted.UseVisualStyleBackColor = true;
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.Location = new System.Drawing.Point(138, 158);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(149, 20);
            this.lblProject.TabIndex = 57;
            this.lblProject.Text = "No Project Selected";
            // 
            // groupboxPriority
            // 
            this.groupboxPriority.Controls.Add(this.rbPriorityHigh);
            this.groupboxPriority.Controls.Add(this.rbPriorityNormal);
            this.groupboxPriority.Controls.Add(this.rbPriorityLow);
            this.groupboxPriority.Location = new System.Drawing.Point(142, 246);
            this.groupboxPriority.Name = "groupboxPriority";
            this.groupboxPriority.Size = new System.Drawing.Size(561, 53);
            this.groupboxPriority.TabIndex = 63;
            this.groupboxPriority.TabStop = false;
            this.groupboxPriority.Text = "Priority";
            // 
            // rbPriorityHigh
            // 
            this.rbPriorityHigh.AutoSize = true;
            this.rbPriorityHigh.Location = new System.Drawing.Point(291, 23);
            this.rbPriorityHigh.Name = "rbPriorityHigh";
            this.rbPriorityHigh.Size = new System.Drawing.Size(67, 24);
            this.rbPriorityHigh.TabIndex = 2;
            this.rbPriorityHigh.TabStop = true;
            this.rbPriorityHigh.Text = "High";
            this.rbPriorityHigh.UseVisualStyleBackColor = true;
            // 
            // rbPriorityNormal
            // 
            this.rbPriorityNormal.AutoSize = true;
            this.rbPriorityNormal.Location = new System.Drawing.Point(158, 25);
            this.rbPriorityNormal.Name = "rbPriorityNormal";
            this.rbPriorityNormal.Size = new System.Drawing.Size(84, 24);
            this.rbPriorityNormal.TabIndex = 1;
            this.rbPriorityNormal.TabStop = true;
            this.rbPriorityNormal.Text = "Normal";
            this.rbPriorityNormal.UseVisualStyleBackColor = true;
            // 
            // rbPriorityLow
            // 
            this.rbPriorityLow.AutoSize = true;
            this.rbPriorityLow.Location = new System.Drawing.Point(25, 25);
            this.rbPriorityLow.Name = "rbPriorityLow";
            this.rbPriorityLow.Size = new System.Drawing.Size(63, 24);
            this.rbPriorityLow.TabIndex = 0;
            this.rbPriorityLow.TabStop = true;
            this.rbPriorityLow.Text = "Low";
            this.rbPriorityLow.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 470);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 20);
            this.label10.TabIndex = 61;
            this.label10.Text = "Code";
            // 
            // lblCreatedBy
            // 
            this.lblCreatedBy.AutoSize = true;
            this.lblCreatedBy.Location = new System.Drawing.Point(142, 339);
            this.lblCreatedBy.Name = "lblCreatedBy";
            this.lblCreatedBy.Size = new System.Drawing.Size(88, 20);
            this.lblCreatedBy.TabIndex = 60;
            this.lblCreatedBy.Text = "Created By";
            // 
            // dateResolutionDate
            // 
            this.dateResolutionDate.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            this.dateResolutionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateResolutionDate.Location = new System.Drawing.Point(146, 305);
            this.dateResolutionDate.Name = "dateResolutionDate";
            this.dateResolutionDate.Size = new System.Drawing.Size(200, 26);
            this.dateResolutionDate.TabIndex = 59;
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.Location = new System.Drawing.Point(142, 435);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.Size = new System.Drawing.Size(179, 26);
            this.txtSourceFile.TabIndex = 70;
            // 
            // txtClass
            // 
            this.txtClass.Location = new System.Drawing.Point(142, 402);
            this.txtClass.Name = "txtClass";
            this.txtClass.Size = new System.Drawing.Size(179, 26);
            this.txtClass.TabIndex = 69;
            // 
            // txtMethod
            // 
            this.txtMethod.Location = new System.Drawing.Point(142, 369);
            this.txtMethod.Name = "txtMethod";
            this.txtMethod.Size = new System.Drawing.Size(179, 26);
            this.txtMethod.TabIndex = 68;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 430);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 20);
            this.label14.TabIndex = 67;
            this.label14.Text = "Source File";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 399);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 20);
            this.label13.TabIndex = 66;
            this.label13.Text = "Class";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 369);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 20);
            this.label12.TabIndex = 65;
            this.label12.Text = "Method";
            // 
            // txtCode
            // 
            this.txtCode.IsReadOnly = false;
            this.txtCode.Location = new System.Drawing.Point(14, 509);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(1500, 460);
            this.txtCode.TabIndex = 64;
            this.txtCode.Load += new System.EventHandler(this.txtCode_Load);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 246);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 20);
            this.label11.TabIndex = 62;
            this.label11.Text = "Priority";
            // 
            // btnSelectProject
            // 
            this.btnSelectProject.Location = new System.Drawing.Point(305, 158);
            this.btnSelectProject.Name = "btnSelectProject";
            this.btnSelectProject.Size = new System.Drawing.Size(135, 35);
            this.btnSelectProject.TabIndex = 56;
            this.btnSelectProject.Text = "Select Project";
            this.btnSelectProject.UseVisualStyleBackColor = true;
            this.btnSelectProject.Click += new System.EventHandler(this.btnSelectProject_Click);
            // 
            // lblAssignedTo
            // 
            this.lblAssignedTo.AutoSize = true;
            this.lblAssignedTo.Location = new System.Drawing.Point(641, 93);
            this.lblAssignedTo.Name = "lblAssignedTo";
            this.lblAssignedTo.Size = new System.Drawing.Size(134, 20);
            this.lblAssignedTo.TabIndex = 55;
            this.lblAssignedTo.Text = "No User Selected";
            // 
            // btnSelectAssignedTo
            // 
            this.btnSelectAssignedTo.Location = new System.Drawing.Point(796, 89);
            this.btnSelectAssignedTo.Name = "btnSelectAssignedTo";
            this.btnSelectAssignedTo.Size = new System.Drawing.Size(135, 35);
            this.btnSelectAssignedTo.TabIndex = 54;
            this.btnSelectAssignedTo.Text = "Select User";
            this.btnSelectAssignedTo.UseVisualStyleBackColor = true;
            this.btnSelectAssignedTo.Click += new System.EventHandler(this.btnSelectAssignedTo_Click);
            // 
            // btnSaveBug
            // 
            this.btnSaveBug.Location = new System.Drawing.Point(534, 376);
            this.btnSaveBug.Name = "btnSaveBug";
            this.btnSaveBug.Size = new System.Drawing.Size(192, 78);
            this.btnSaveBug.TabIndex = 53;
            this.btnSaveBug.Text = "Save Bug";
            this.btnSaveBug.UseVisualStyleBackColor = true;
            this.btnSaveBug.Click += new System.EventHandler(this.btnSaveBug_Click);
            // 
            // dateIdentifiedOn
            // 
            this.dateIdentifiedOn.CustomFormat = "MM/dd/yyyy hh:mm:ss";
            this.dateIdentifiedOn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateIdentifiedOn.Location = new System.Drawing.Point(142, 124);
            this.dateIdentifiedOn.Name = "dateIdentifiedOn";
            this.dateIdentifiedOn.Size = new System.Drawing.Size(297, 26);
            this.dateIdentifiedOn.TabIndex = 52;
            // 
            // lblIdentifiedBy
            // 
            this.lblIdentifiedBy.AutoSize = true;
            this.lblIdentifiedBy.Location = new System.Drawing.Point(142, 92);
            this.lblIdentifiedBy.Name = "lblIdentifiedBy";
            this.lblIdentifiedBy.Size = new System.Drawing.Size(134, 20);
            this.lblIdentifiedBy.TabIndex = 51;
            this.lblIdentifiedBy.Text = "No User Selected";
            // 
            // btnSelectIdentifiedBy
            // 
            this.btnSelectIdentifiedBy.Location = new System.Drawing.Point(305, 89);
            this.btnSelectIdentifiedBy.Name = "btnSelectIdentifiedBy";
            this.btnSelectIdentifiedBy.Size = new System.Drawing.Size(135, 35);
            this.btnSelectIdentifiedBy.TabIndex = 50;
            this.btnSelectIdentifiedBy.Text = "Select User";
            this.btnSelectIdentifiedBy.UseVisualStyleBackColor = true;
            this.btnSelectIdentifiedBy.Click += new System.EventHandler(this.btnSelectIdentifiedBy_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(142, 56);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(402, 26);
            this.txtDescription.TabIndex = 49;
            // 
            // txtSummary
            // 
            this.txtSummary.Location = new System.Drawing.Point(142, 25);
            this.txtSummary.Name = "txtSummary";
            this.txtSummary.Size = new System.Drawing.Size(402, 26);
            this.txtSummary.TabIndex = 48;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 339);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 20);
            this.label9.TabIndex = 47;
            this.label9.Text = "Created By";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 304);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 20);
            this.label8.TabIndex = 46;
            this.label8.Text = "Resolution Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 20);
            this.label7.TabIndex = 45;
            this.label7.Text = "Progress";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "Project";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(516, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 20);
            this.label5.TabIndex = 43;
            this.label5.Text = "Assigned To";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 42;
            this.label4.Text = "Identified On";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 20);
            this.label3.TabIndex = 41;
            this.label3.Text = "Identified By";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 20);
            this.label2.TabIndex = 40;
            this.label2.Text = "Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 20);
            this.label1.TabIndex = 39;
            this.label1.Text = "Summary";
            // 
            // NewBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 1028);
            this.Controls.Add(this.groupBox1);
            this.Name = "NewBug";
            this.Text = "NewBug";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NewBug_FormClosed);
            this.Load += new System.EventHandler(this.NewBug_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupProgress.ResumeLayout(false);
            this.groupProgress.PerformLayout();
            this.groupboxPriority.ResumeLayout(false);
            this.groupboxPriority.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupProgress;
        private System.Windows.Forms.RadioButton rbProgressCompleted;
        private System.Windows.Forms.RadioButton rbProgressInProgress;
        private System.Windows.Forms.RadioButton rbProgressNotStarted;
        private System.Windows.Forms.Label lblProject;
        private System.Windows.Forms.GroupBox groupboxPriority;
        private System.Windows.Forms.RadioButton rbPriorityHigh;
        private System.Windows.Forms.RadioButton rbPriorityNormal;
        private System.Windows.Forms.RadioButton rbPriorityLow;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCreatedBy;
        private System.Windows.Forms.DateTimePicker dateResolutionDate;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.TextBox txtMethod;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private ICSharpCode.TextEditor.TextEditorControl txtCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnSelectProject;
        private System.Windows.Forms.Label lblAssignedTo;
        private System.Windows.Forms.Button btnSelectAssignedTo;
        private System.Windows.Forms.Button btnSaveBug;
        private System.Windows.Forms.DateTimePicker dateIdentifiedOn;
        private System.Windows.Forms.Label lblIdentifiedBy;
        private System.Windows.Forms.Button btnSelectIdentifiedBy;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtSummary;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}