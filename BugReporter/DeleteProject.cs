﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class DeleteProject : Form
    {
        SqlConnection SqlConnection;
        public DeleteProject()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30");

        }

        private void DeleteProject_Load(object sender, EventArgs e)
        {
            populateListBox();
        }

        private void populateListBox()
        {
            String selcmd = "SELECT PROJECTID, NAME, CREATED_BY, TARGET_END, START, CREATED_ON FROM PROJECTS ORDER BY PROJECTID";

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["PROJECTID"].ToString(), SqlDataReader["NAME"].ToString(), SqlDataReader["CREATED_BY"].ToString(), SqlDataReader["START"].ToString(), SqlDataReader["TARGET_END"].ToString(), SqlDataReader["CREATED_ON"].ToString() };
                    var listViewItem = new ListViewItem(row);
                    listView1.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void DeleteProject_FormClosed(object sender, FormClosedEventArgs e)
        {
            BugMenu BugMenu = new BugMenu();
            BugMenu.Show();
        }

        private void btnDeleteProject_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];

                String selcmd = "DELETE FROM PROJECTS WHERE PROJECTID = @SELECTEDID";
                deleterecord(item.SubItems[0].Text, selcmd);
                MessageBox.Show("Record Deleted");
                listView1.Items.Clear();
                populateListBox();

            }
            else
            {
                MessageBox.Show("Please Select a Record");
            }

        }

        public void deleterecord(String selectedID, String commandString)
        {
            try
            {

                SqlConnection.Open();

                SqlCommand cmdDelete = new SqlCommand(commandString, SqlConnection);

                cmdDelete.Parameters.AddWithValue("@SELECTEDID", selectedID);
                cmdDelete.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SqlConnection.Close();

        }
    }
}
