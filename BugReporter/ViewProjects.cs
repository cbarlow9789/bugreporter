﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class ViewProjects : Form
    {
        SqlConnection SqlConnection;
        public ViewProjects()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30");

        }

        private void ViewProjects_Load(object sender, EventArgs e)
        {
            populateListBox();
        }

        private void populateListBox()
        {
            String selcmd = "SELECT PROJECTID, NAME, CREATED_BY, TARGET_END, START, CREATED_ON FROM PROJECTS ORDER BY PROJECTID";

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["NAME"].ToString(), SqlDataReader["CREATED_BY"].ToString(), SqlDataReader["START"].ToString(), SqlDataReader["TARGET_END"].ToString(), SqlDataReader["CREATED_ON"].ToString() };
                    var listViewItem = new ListViewItem(row);
                    listView1.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void ViewProjects_FormClosed(object sender, FormClosedEventArgs e)
        {
            BugMenu BugMenu = new BugMenu();
            BugMenu.Show();
        }
    }
}
