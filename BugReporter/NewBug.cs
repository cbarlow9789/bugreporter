﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class NewBug : Form
    {
        int IdentifiedByID;
        int AssignedToID;
        int ProjectID;
        int OriginalBugID;
        public String Priority = null;
        public String Progress = null;
        SqlConnection SqlConnection;
        public NewBug()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");
        }

        private void btnSelectIdentifiedBy_Click(object sender, EventArgs e)
        {
            using (SelectUser SelectUser = new SelectUser())
            {
                if (SelectUser.ShowDialog() == DialogResult.OK)
                {
                    IdentifiedByID = SelectUser.User;
                    lblIdentifiedBy.Text = SelectUser.userName;
                }
            }
        }

        private void btnSelectAssignedTo_Click(object sender, EventArgs e)
        {
            using (SelectUser SelectUser = new SelectUser())
            {
                if (SelectUser.ShowDialog() == DialogResult.OK)
                {
                    AssignedToID = SelectUser.User;
                    lblAssignedTo.Text = SelectUser.userName;
                }
            }
        }

        private void btnSelectProject_Click(object sender, EventArgs e)
        {
            using (SelectProject SelectProject = new SelectProject())
            {
                if (SelectProject.ShowDialog() == DialogResult.OK)
                {
                    ProjectID = SelectProject.Project;
                    lblProject.Text = SelectProject.ProjectName;
                }
            }
        }

        private void NewBug_Load(object sender, EventArgs e)
        {
            lblCreatedBy.Text = Variables.userName;
        }

        private void nextSeq()
        {
            String selcmd = "SELECT NEXT VALUE FOR[dbo].[Bug_BUGID_SEQUENCE] +1 AS Seq";

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    OriginalBugID = Int32.Parse(SqlDataReader["Seq"].ToString());
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void btnSaveBug_Click(object sender, EventArgs e)
        {
            DateTime IdentifiedOn = dateIdentifiedOn.Value;
            DateTime ResolutionDate = dateResolutionDate.Value;

            int AuditID = 1;

            if (rbPriorityLow.Checked)
            {
                Priority = "LOW";
            }
            else if (rbPriorityNormal.Checked)
            {
                Priority = "NORMAL";
            }
            else if (rbPriorityHigh.Checked)
            {
                Priority = "HIGH";
            }

            if (rbProgressNotStarted.Checked)
            {
                Progress = "NOT STARTED";
            }
            else if (rbProgressInProgress.Checked)
            {
                Progress = "IN PROGRESS";
            }
            else if (rbProgressCompleted.Checked)
            {
                Progress = "COMPLETED";
            }


            if (checkInputs())
            {
                nextSeq();
                String commandString = "INSERT INTO BUG(SUMMARY, DESCRIPTION, IDENTIFIED_BY, IDENTIFIED_ON, ASSIGNED_TO, PROJECTID, PRIORITY, PROGRESS, RESOLUTION_DATE, CREATED_BY, MODIFIED_BY, AUDIT_ID, CODE, METHOD, CLASS, SOURCEFILE, ORIGINAL_BUGID) VALUES (@SUMMARY, @DESCRIPTION, @IDENTIFIED_BY, @IDENTIFIED_ON, @ASSIGNED_TO, @PROJECTID, @PRIORITY, @PROGRESS, @RESOLUTION_DATE, @CREATED_BY, @MODIFIED_BY, @AUDIT_ID, @CODE, @METHOD, @CLASS, @SOURCEFILE, @ORIGINALBUGID)";
                insertRecord(txtSummary.Text, txtDescription.Text, IdentifiedByID, IdentifiedOn, AssignedToID, ProjectID, Priority, Progress, ResolutionDate, Variables.userID, Variables.userID, AuditID, txtCode.Text, txtMethod.Text, txtClass.Text, txtSourceFile.Text, OriginalBugID, commandString);
                MessageBox.Show("Success: Bug Successfully Added");
                ClearInputs();

            }
        }

        public bool insertRecord(String Summary, String Description, int IdentifiedBy, DateTime IdentifiedOn, int AssignedToID, int ProjectID, String Priority, String Progress, DateTime ResolutionDate, int CreatedBy, int ModifiedBy, int AuditID, String Code, String Method, String Class, String SourceFile, int OriginalBugID, String commandString)
        {

            try
            {

                SqlConnection.Open();

                SqlCommand cmdInsert = new SqlCommand(commandString, SqlConnection);

                cmdInsert.Parameters.AddWithValue("@SUMMARY", Summary);
                cmdInsert.Parameters.AddWithValue("@DESCRIPTION", Description);
                cmdInsert.Parameters.AddWithValue("@IDENTIFIED_BY", IdentifiedBy);
                cmdInsert.Parameters.AddWithValue("@IDENTIFIED_ON", IdentifiedOn);
                cmdInsert.Parameters.AddWithValue("@ASSIGNED_TO", AssignedToID);
                cmdInsert.Parameters.AddWithValue("@PROJECTID", ProjectID);
                cmdInsert.Parameters.AddWithValue("@PRIORITY", Priority);
                cmdInsert.Parameters.AddWithValue("@PROGRESS", Progress);
                cmdInsert.Parameters.AddWithValue("@RESOLUTION_DATE", ResolutionDate);
                cmdInsert.Parameters.AddWithValue("@CREATED_BY", CreatedBy);
                cmdInsert.Parameters.AddWithValue("@MODIFIED_BY", ModifiedBy);
                cmdInsert.Parameters.AddWithValue("@AUDIT_ID", AuditID);
                cmdInsert.Parameters.AddWithValue("@CODE", Code);
                cmdInsert.Parameters.AddWithValue("@METHOD", Method);
                cmdInsert.Parameters.AddWithValue("@CLASS", Class);
                cmdInsert.Parameters.AddWithValue("@SOURCEFILE", SourceFile);
                cmdInsert.Parameters.AddWithValue("@ORIGINALBUGID", OriginalBugID);

                cmdInsert.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            SqlConnection.Close();

        }

        public bool checkInputs()
        {
            bool rtnvalue = true;
            if (string.IsNullOrEmpty(txtSummary.Text))
            {
                MessageBox.Show("Error: Please Enter Summary");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtDescription.Text))
            {
                MessageBox.Show("Error: Please Enter Description");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtMethod.Text))
            {
                MessageBox.Show("Error: Please Enter Method");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtClass.Text))
            {
                MessageBox.Show("Error: Please Enter Class");
                rtnvalue = false;
            }
            if (string.IsNullOrEmpty(txtSourceFile.Text))
            {
                MessageBox.Show("Error: Please Enter SourceFile");
                rtnvalue = false;
            }
            if (dateResolutionDate.Value < DateTime.Now)
            {
                MessageBox.Show("Error: Resolution Date cannot be before Current Date");
                rtnvalue = false;
            }
            if (Progress == null)
            {
                MessageBox.Show("Error: Please Select Progress Value");
                rtnvalue = false;
            }
            if (Priority == null)
            {
                MessageBox.Show("Error: Please Select Priority Value");
                rtnvalue = false;
            }
            if (AssignedToID == 0)
            {
                MessageBox.Show("Error: Please Assign Bug to a User");
                rtnvalue = false;
            }
            if (ProjectID == 0)
            {
                MessageBox.Show("Error: Please Select Project where Bug is Located");
                rtnvalue = false;
            }
            if (IdentifiedByID == 0)
            {
                MessageBox.Show("Error: Please Select who Identified the Bug");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void ClearInputs()
        {
            txtSummary.Text = "";
            txtDescription.Text = "";
            txtMethod.Text = "";
            txtClass.Text = "";
            txtSourceFile.Text = "";
            rbProgressInProgress.Checked = false;
            rbProgressCompleted.Checked = false;
            rbProgressNotStarted.Checked = false;
            AssignedToID = 0;
            ProjectID = 0;
            OriginalBugID = 0;
            lblIdentifiedBy.Text = "No User Selected";
            lblAssignedTo.Text = "No User Selected";
            lblProject.Text = "No Project Selected";
            txtCode.Text = "";

        }

        private void NewBug_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            BugMenu BugMenu = new BugMenu();
            BugMenu.Show();
        }

        private void txtCode_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {
                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtCode.SetHighlighting("C#");
            }
        }
    }
}
