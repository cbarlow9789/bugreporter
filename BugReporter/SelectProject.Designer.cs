﻿namespace BugReporter
{
    partial class SelectProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listProjects = new System.Windows.Forms.ListView();
            this.columnProjectID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnCreatedOn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnTargetEnd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listProjects
            // 
            this.listProjects.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnProjectID,
            this.columnName,
            this.columnCreatedOn,
            this.columnTargetEnd});
            this.listProjects.FullRowSelect = true;
            this.listProjects.Location = new System.Drawing.Point(12, 25);
            this.listProjects.Name = "listProjects";
            this.listProjects.Size = new System.Drawing.Size(517, 370);
            this.listProjects.TabIndex = 4;
            this.listProjects.UseCompatibleStateImageBehavior = false;
            this.listProjects.View = System.Windows.Forms.View.Details;
            this.listProjects.SelectedIndexChanged += new System.EventHandler(this.listProjects_SelectedIndexChanged);
            // 
            // columnProjectID
            // 
            this.columnProjectID.Text = "ProjectID";
            this.columnProjectID.Width = 85;
            // 
            // columnName
            // 
            this.columnName.Tag = "";
            this.columnName.Text = "Name";
            this.columnName.Width = 110;
            // 
            // columnCreatedOn
            // 
            this.columnCreatedOn.Text = "Created On";
            // 
            // columnTargetEnd
            // 
            this.columnTargetEnd.Text = "Target End";
            // 
            // SelectProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 416);
            this.Controls.Add(this.listProjects);
            this.Name = "SelectProject";
            this.Text = "SelectProject";
            this.Load += new System.EventHandler(this.SelectProject_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listProjects;
        private System.Windows.Forms.ColumnHeader columnProjectID;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnCreatedOn;
        private System.Windows.Forms.ColumnHeader columnTargetEnd;
    }
}