﻿using ICSharpCode.TextEditor.Document;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class BugAudits : Form
    {
        SqlConnection SqlConnection;
        int OriginalBugID;
        int AuditID;
        int IdentifiedByID;
        int AssignedToID;
        int CreatedByID;
        int ProjectID;

        public BugAudits()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30;MultipleActiveResultSets=true");
            InitializeComponent();
        }

        private void BugAudits_Load(object sender, EventArgs e)
        {
            using (ViewBugs ViewBugs = new ViewBugs())
            OriginalBugID = ViewBugs._OriginalBugID;
            populateComboBox();
            
        }

        private void populateComboBox()
        {
            String selcmd = "SELECT * FROM BUG WHERE ORIGINAL_BUGID = " + OriginalBugID;

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    String Audit = SqlDataReader["AUDIT_ID"].ToString() + " - " + SqlDataReader["PROGRESS"];
                    comboBugVersion.Items.Add(Audit);
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void txtCode_Load(object sender, EventArgs e)
        {
            string dirc = Application.StartupPath;
            FileSyntaxModeProvider fsmp;
            if (Directory.Exists(dirc))
            {
                fsmp = new FileSyntaxModeProvider(dirc);
                HighlightingManager.Manager.AddSyntaxModeFileProvider(fsmp);
                txtCode.SetHighlighting("C#");
            }
        }

        private void comboBugVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCode.Text = " ";
            String temp = comboBugVersion.Text.Split(' ').First();
            AuditID = Int32.Parse(temp);

            String selcmd = "SELECT * FROM BUG WHERE ORIGINAL_BUGID = " + OriginalBugID + " AND AUDIT_ID = " + AuditID;

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblSummary.Text = "Summary: " + SqlDataReader["SUMMARY"].ToString();
                    lblDescription.Text = "Description: " + SqlDataReader["Description"];

                    IdentifiedByID = Int32.Parse(SqlDataReader["IDENTIFIED_BY"].ToString());
                    AssignedToID = Int32.Parse(SqlDataReader["ASSIGNED_TO"].ToString());
                    CreatedByID = Int32.Parse(SqlDataReader["CREATED_BY"].ToString());
                    ProjectID = Int32.Parse(SqlDataReader["PROJECTID"].ToString());

                    lblIdentifiedOn.Text = "Identified On: " + SqlDataReader["IDENTIFIED_ON"];
                    lblPriority.Text = "Priority: " + SqlDataReader["PRIORITY"];
                    lblProgress.Text = "Progress: " + SqlDataReader["PROGRESS"];
                    lblResolutionDate.Text = "Resolution Date: " + SqlDataReader["RESOLUTION_DATE"];
                    lblMethod.Text = "Method: " + SqlDataReader["METHOD"];
                    lblClass.Text = "Class: " + SqlDataReader["CLASS"];
                    lblSourceFile.Text = "SourceFile: " + SqlDataReader["SOURCEFILE"];
                    txtCode.Text = SqlDataReader["CODE"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();
            GetNames();

        }

        private void GetNames()
        {
           //IDENTIFIED_BY
            String selcmd = "SELECT NAME, ROLE FROM USERS WHERE USERID = " + IdentifiedByID;

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblIdentifiedBy.Text = "Identified By: " + SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            

            //ASSIGNED_TO
            selcmd = "SELECT NAME, ROLE FROM USERS WHERE USERID = " + AssignedToID;

            mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblAssignedTo.Text = "Assigned to: " + SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

  

            //CREATED_BY
            selcmd = "SELECT NAME, ROLE FROM USERS WHERE USERID = " + CreatedByID;

            mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblCreatedBy.Text = "Created By: " + SqlDataReader["NAME"].ToString() + " - " + SqlDataReader["ROLE"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //PROJECT
            selcmd = "SELECT NAME FROM PROJECTS WHERE PROJECTID = " + ProjectID;

            mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    lblProject.Text = "Project: " + SqlDataReader["NAME"].ToString();
                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void BugAudits_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            ViewBugs ViewBugs = new ViewBugs();
            ViewBugs.Show();
        }
    }
 }

