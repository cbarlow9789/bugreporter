﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class EditProject : Form
    {
        SqlConnection SqlConnection;
        public EditProject()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30");

        }

        private void EditProject_FormClosed(object sender, FormClosedEventArgs e)
        {
            BugMenu BugMenu = new BugMenu();
            BugMenu.Show();
        }

        private void EditProject_Load(object sender, EventArgs e)
        {
            populateListBox();
        }

        private void populateListBox()
        {
            String selcmd = "SELECT PROJECTID, NAME, CREATED_BY, TARGET_END, START, CREATED_ON FROM PROJECTS ORDER BY PROJECTID";

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["PROJECTID"].ToString(), SqlDataReader["NAME"].ToString(), SqlDataReader["CREATED_BY"].ToString(), SqlDataReader["START"].ToString(), SqlDataReader["TARGET_END"].ToString(), SqlDataReader["CREATED_ON"].ToString() };
                    var listViewItem = new ListViewItem(row);
                    listView1.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listView1.SelectedItems.Count > 0)
            {
                ListViewItem item = listView1.SelectedItems[0];

                String selcmd = "SELECT PROJECTID, NAME, CREATED_BY, TARGET_END, START, CREATED_ON FROM PROJECTS WHERE PROJECTID = @SELECTEDID";
                displayrecord(item.SubItems[0].Text, selcmd);
                
            }


        }

        public void displayrecord(String selectedID, String commandString)
        {
            try
            {

                SqlConnection.Open();

                SqlCommand cmdDisplay = new SqlCommand(commandString, SqlConnection);

                cmdDisplay.Parameters.AddWithValue("@SELECTEDID", selectedID);

                SqlDataReader SqlDataReader = cmdDisplay.ExecuteReader();

                while (SqlDataReader.Read())
                {
                    txtName.Text = SqlDataReader["NAME"].ToString();
                    txtCreatedby.Text = SqlDataReader["CREATED_BY"].ToString();
                    dateStart.Value = (DateTime)SqlDataReader["START"];
                    dateEnd.Value = (DateTime)SqlDataReader["TARGET_END"];

                }


            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SqlConnection.Close();
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "UPDATE PROJECTS SET NAME = @NAME, TARGET_END = @TARGET_END, START = @START WHERE PROJECTID = @PROJECTID; ";
                ListViewItem item = listView1.SelectedItems[0];
                updateRecord(txtName.Text, dateEnd.Value, dateStart.Value, Int32.Parse(item.SubItems[0].Text), commandString);
                MessageBox.Show("Project Updated");
                listView1.Items.Clear();
                populateListBox();
            }
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtName.Text))

            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void updateRecord(String name, DateTime dateEnd, DateTime dateStart, int selectedID ,String commandString)
        {
            try
            {

                SqlConnection.Open();

                SqlCommand cmdUpdate = new SqlCommand(commandString, SqlConnection);

                cmdUpdate.Parameters.AddWithValue("@NAME", name);
                cmdUpdate.Parameters.AddWithValue("@TARGET_END", dateEnd);
                cmdUpdate.Parameters.AddWithValue("@START", dateStart);
                cmdUpdate.Parameters.AddWithValue("@PROJECTID", selectedID);

                cmdUpdate.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            SqlConnection.Close();
        }
    }
}

