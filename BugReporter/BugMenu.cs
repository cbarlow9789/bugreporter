﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class BugMenu : Form
    {
        SqlConnection SqlConnection;
        public BugMenu()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\Users\conor\OneDrive\Documents\Uni Work\Software Engineering\BugReporter\BugReporter\BugReporter\database.mdf';Integrated Security=True;Connect Timeout=30;MultipleActiveResultSets=True;");
            InitializeComponent();
            label1.Text = "Logged in user ID: " + Variables.userID.ToString();
        }

        private void BugMenu_Load(object sender, EventArgs e)
        {
            populateMyBugs();
            populatePriorityBugs();
        }

        private void BugMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            login login = new login();
            login.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            login login = new login();
            login.Show();

        }

        private void addProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            AddProject addproject = new AddProject();
            addproject.Show();

        }

        private void viewProjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ViewProjects viewproject = new ViewProjects();
            viewproject.Show();
        }

        private void editProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            EditProject EditProject = new EditProject();
            EditProject.Show();
        }

        private void deleteProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            DeleteProject DeleteProject = new DeleteProject();
            DeleteProject.Show();
        }

        private void reportNewBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            NewBug NewBug = new NewBug();
            NewBug.Show();
        }

        private void viewBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ViewBugs ViewBugs = new ViewBugs();
            ViewBugs.Show();
        }

        private void updateBugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            ViewMyBugs ViewMyBugs = new ViewMyBugs();
            ViewMyBugs.Show();
        }

        private void populateMyBugs()
        {
            String selcmd = "SELECT * FROM BUG WHERE AUDIT_ID = 1 AND ASSIGNED_TO = " + Variables.userID;

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["ORIGINAL_BUGID"].ToString(), SqlDataReader["SUMMARY"].ToString(), SqlDataReader["IDENTIFIED_ON"].ToString(), SqlDataReader["PRIORITY"].ToString(), SqlDataReader["SOURCEFILE"].ToString() };
                    var listViewItem = new ListViewItem(row);

                    String selcmd2 = "SELECT * FROM BUG";

                    SqlCommand mySqlCommand2 = new SqlCommand(selcmd2, SqlConnection);

                    try
                    {

                        SqlDataReader SqlDataReader2 = mySqlCommand2.ExecuteReader();

                        while (SqlDataReader2.Read())
                        {
                            if (SqlDataReader2["PROGRESS"].ToString() == "COMPLETED" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Green;
                            }
                            else if (SqlDataReader2["PROGRESS"].ToString() == "IN PROGRESS" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Orange;
                            }
                            else if (SqlDataReader2["PROGRESS"].ToString() == "NOT STARTED" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Red;
                            }
                        }
                    }

                    catch (SqlException ex)
                    {

                        MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    listBugs.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

        private void populatePriorityBugs()
        {
            String selcmd = "SELECT * FROM BUG WHERE PRIORITY = 'HIGH' and AUDIT_ID = 1";

            SqlCommand mySqlCommand = new SqlCommand(selcmd, SqlConnection);

            try
            {
                SqlConnection.Open();

                SqlDataReader SqlDataReader = mySqlCommand.ExecuteReader();

                while (SqlDataReader.Read())
                {

                    string[] row = { SqlDataReader["PRIORITY"].ToString(), SqlDataReader["ORIGINAL_BUGID"].ToString(), SqlDataReader["SUMMARY"].ToString(), SqlDataReader["IDENTIFIED_ON"].ToString(), SqlDataReader["SOURCEFILE"].ToString() };
                    var listViewItem = new ListViewItem(row);

                    String selcmd2 = "SELECT * FROM BUG";

                    SqlCommand mySqlCommand2 = new SqlCommand(selcmd2, SqlConnection);

                    try
                    {

                        SqlDataReader SqlDataReader2 = mySqlCommand2.ExecuteReader();

                        while (SqlDataReader2.Read())
                        {
                            if (SqlDataReader2["PROGRESS"].ToString() == "COMPLETED" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Green;
                            }
                            else if (SqlDataReader2["PROGRESS"].ToString() == "IN PROGRESS" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Orange;
                            }
                            else if (SqlDataReader2["PROGRESS"].ToString() == "NOT STARTED" && SqlDataReader2["ORIGINAL_BUGID"].ToString() == SqlDataReader["ORIGINAL_BUGID"].ToString())
                            {
                                listViewItem.BackColor = Color.Red;
                            }
                        }
                    }

                    catch (SqlException ex)
                    {

                        MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    listRecent5.Items.Add(listViewItem);


                }
            }

            catch (SqlException ex)
            {

                MessageBox.Show(" .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            SqlConnection.Close();

        }

    }
}