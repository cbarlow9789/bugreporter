﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BugReporter
{
    public partial class AddProject : Form
    {
        SqlConnection SqlConnection;
        public AddProject()
        {
            InitializeComponent();
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");

        }

        private void project_Load(object sender, EventArgs e)
        {
            txtCreatedby.Text = Variables.userID.ToString();
        }

        private void btnAddproject_Click(object sender, EventArgs e)
        {
             if (checkInputs())
            {

                String commandString = "INSERT INTO PROJECTS(NAME, CREATED_BY, TARGET_END, START) VALUES (@name, @created_by, @target_end, @start)";

                insertRecord(txtName.Text, Variables.userID, dateEnd.Value, dateStart.Value, commandString);
            }
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtName.Text))
                
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public bool insertRecord(String name, int created_by, DateTime target_end, DateTime start, String commandString)
        {

            try
            {

                SqlConnection.Open();

                SqlCommand cmdInsert = new SqlCommand(commandString, SqlConnection);

                cmdInsert.Parameters.AddWithValue("@NAME", name);
                cmdInsert.Parameters.AddWithValue("@CREATED_BY", created_by);
                cmdInsert.Parameters.AddWithValue("@TARGET_END", target_end);
                cmdInsert.Parameters.AddWithValue("@START", start);
                cmdInsert.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }
            SqlConnection.Close();

        }

        private void AddProject_FormClosed(object sender, FormClosedEventArgs e)
        {
            BugMenu BugMenu = new BugMenu();
            BugMenu.Show();
        }
    }
}
