﻿namespace BugReporter
{
    partial class UpdateBug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAuditID = new System.Windows.Forms.Label();
            this.txtAuditCode = new ICSharpCode.TextEditor.TextEditorControl();
            this.lblSourceFile = new System.Windows.Forms.Label();
            this.lblClass = new System.Windows.Forms.Label();
            this.lblMethod = new System.Windows.Forms.Label();
            this.lblPriority = new System.Windows.Forms.Label();
            this.lblCreatedBy = new System.Windows.Forms.Label();
            this.lblResolutionDate = new System.Windows.Forms.Label();
            this.lblProgress = new System.Windows.Forms.Label();
            this.lblProject = new System.Windows.Forms.Label();
            this.lblAssignedTo = new System.Windows.Forms.Label();
            this.lblIdentifiedOn = new System.Windows.Forms.Label();
            this.lblIdentifiedBy = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblSummary = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAuditID = new System.Windows.Forms.TextBox();
            this.txtSourceFile = new System.Windows.Forms.TextBox();
            this.txtMethod = new System.Windows.Forms.TextBox();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.txtCreatedBy = new System.Windows.Forms.TextBox();
            this.dateResolutionDate = new System.Windows.Forms.DateTimePicker();
            this.txtPriority = new System.Windows.Forms.TextBox();
            this.groupProgress = new System.Windows.Forms.GroupBox();
            this.rbProgressCompleted = new System.Windows.Forms.RadioButton();
            this.rbProgressInProgress = new System.Windows.Forms.RadioButton();
            this.rbProgressNotStarted = new System.Windows.Forms.RadioButton();
            this.txtProject = new System.Windows.Forms.TextBox();
            this.txtAssignedTo = new System.Windows.Forms.TextBox();
            this.txtIdentifiedBy = new System.Windows.Forms.TextBox();
            this.txtIdentifiedOn = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.txtSummary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCode = new ICSharpCode.TextEditor.TextEditorControl();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnUpdateBug = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.lblAuditID);
            this.groupBox1.Controls.Add(this.txtAuditCode);
            this.groupBox1.Controls.Add(this.lblSourceFile);
            this.groupBox1.Controls.Add(this.lblClass);
            this.groupBox1.Controls.Add(this.lblMethod);
            this.groupBox1.Controls.Add(this.lblPriority);
            this.groupBox1.Controls.Add(this.lblCreatedBy);
            this.groupBox1.Controls.Add(this.lblResolutionDate);
            this.groupBox1.Controls.Add(this.lblProgress);
            this.groupBox1.Controls.Add(this.lblProject);
            this.groupBox1.Controls.Add(this.lblAssignedTo);
            this.groupBox1.Controls.Add(this.lblIdentifiedOn);
            this.groupBox1.Controls.Add(this.lblIdentifiedBy);
            this.groupBox1.Controls.Add(this.lblDescription);
            this.groupBox1.Controls.Add(this.lblSummary);
            this.groupBox1.Location = new System.Drawing.Point(19, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(527, 827);
            this.groupBox1.TabIndex = 84;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Last Bug Version:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 420);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 100;
            this.label2.Text = "Code:";
            // 
            // lblAuditID
            // 
            this.lblAuditID.AutoSize = true;
            this.lblAuditID.Location = new System.Drawing.Point(17, 29);
            this.lblAuditID.Name = "lblAuditID";
            this.lblAuditID.Size = new System.Drawing.Size(71, 20);
            this.lblAuditID.TabIndex = 98;
            this.lblAuditID.Text = "Audit ID:";
            // 
            // txtAuditCode
            // 
            this.txtAuditCode.Enabled = false;
            this.txtAuditCode.IsReadOnly = false;
            this.txtAuditCode.Location = new System.Drawing.Point(23, 446);
            this.txtAuditCode.Name = "txtAuditCode";
            this.txtAuditCode.Size = new System.Drawing.Size(694, 367);
            this.txtAuditCode.TabIndex = 97;
            this.txtAuditCode.Load += new System.EventHandler(this.txtAuditCode_Load);
            // 
            // lblSourceFile
            // 
            this.lblSourceFile.AutoSize = true;
            this.lblSourceFile.Location = new System.Drawing.Point(22, 389);
            this.lblSourceFile.Name = "lblSourceFile";
            this.lblSourceFile.Size = new System.Drawing.Size(89, 20);
            this.lblSourceFile.TabIndex = 96;
            this.lblSourceFile.Text = "Source File";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Location = new System.Drawing.Point(22, 331);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(48, 20);
            this.lblClass.TabIndex = 95;
            this.lblClass.Text = "Class";
            // 
            // lblMethod
            // 
            this.lblMethod.AutoSize = true;
            this.lblMethod.Location = new System.Drawing.Point(22, 359);
            this.lblMethod.Name = "lblMethod";
            this.lblMethod.Size = new System.Drawing.Size(63, 20);
            this.lblMethod.TabIndex = 94;
            this.lblMethod.Text = "Method";
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Location = new System.Drawing.Point(21, 249);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(56, 20);
            this.lblPriority.TabIndex = 93;
            this.lblPriority.Text = "Priority";
            // 
            // lblCreatedBy
            // 
            this.lblCreatedBy.AutoSize = true;
            this.lblCreatedBy.Location = new System.Drawing.Point(22, 304);
            this.lblCreatedBy.Name = "lblCreatedBy";
            this.lblCreatedBy.Size = new System.Drawing.Size(88, 20);
            this.lblCreatedBy.TabIndex = 92;
            this.lblCreatedBy.Text = "Created By";
            // 
            // lblResolutionDate
            // 
            this.lblResolutionDate.AutoSize = true;
            this.lblResolutionDate.Location = new System.Drawing.Point(21, 276);
            this.lblResolutionDate.Name = "lblResolutionDate";
            this.lblResolutionDate.Size = new System.Drawing.Size(124, 20);
            this.lblResolutionDate.TabIndex = 91;
            this.lblResolutionDate.Text = "Resolution Date";
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Location = new System.Drawing.Point(19, 224);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(72, 20);
            this.lblProgress.TabIndex = 90;
            this.lblProgress.Text = "Progress";
            // 
            // lblProject
            // 
            this.lblProject.AutoSize = true;
            this.lblProject.Location = new System.Drawing.Point(19, 193);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(58, 20);
            this.lblProject.TabIndex = 89;
            this.lblProject.Text = "Project";
            // 
            // lblAssignedTo
            // 
            this.lblAssignedTo.AutoSize = true;
            this.lblAssignedTo.Location = new System.Drawing.Point(18, 166);
            this.lblAssignedTo.Name = "lblAssignedTo";
            this.lblAssignedTo.Size = new System.Drawing.Size(97, 20);
            this.lblAssignedTo.TabIndex = 88;
            this.lblAssignedTo.Text = "Assigned To";
            // 
            // lblIdentifiedOn
            // 
            this.lblIdentifiedOn.AutoSize = true;
            this.lblIdentifiedOn.Location = new System.Drawing.Point(17, 112);
            this.lblIdentifiedOn.Name = "lblIdentifiedOn";
            this.lblIdentifiedOn.Size = new System.Drawing.Size(100, 20);
            this.lblIdentifiedOn.TabIndex = 87;
            this.lblIdentifiedOn.Text = "Identified On";
            // 
            // lblIdentifiedBy
            // 
            this.lblIdentifiedBy.AutoSize = true;
            this.lblIdentifiedBy.Location = new System.Drawing.Point(18, 138);
            this.lblIdentifiedBy.Name = "lblIdentifiedBy";
            this.lblIdentifiedBy.Size = new System.Drawing.Size(97, 20);
            this.lblIdentifiedBy.TabIndex = 86;
            this.lblIdentifiedBy.Text = "Identified By";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(17, 81);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(89, 20);
            this.lblDescription.TabIndex = 85;
            this.lblDescription.Text = "Description";
            // 
            // lblSummary
            // 
            this.lblSummary.AutoSize = true;
            this.lblSummary.Location = new System.Drawing.Point(17, 52);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(80, 20);
            this.lblSummary.TabIndex = 84;
            this.lblSummary.Text = "Summary:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtAuditID);
            this.groupBox2.Controls.Add(this.txtSourceFile);
            this.groupBox2.Controls.Add(this.txtMethod);
            this.groupBox2.Controls.Add(this.txtClass);
            this.groupBox2.Controls.Add(this.txtCreatedBy);
            this.groupBox2.Controls.Add(this.dateResolutionDate);
            this.groupBox2.Controls.Add(this.txtPriority);
            this.groupBox2.Controls.Add(this.groupProgress);
            this.groupBox2.Controls.Add(this.txtProject);
            this.groupBox2.Controls.Add(this.txtAssignedTo);
            this.groupBox2.Controls.Add(this.txtIdentifiedBy);
            this.groupBox2.Controls.Add(this.txtIdentifiedOn);
            this.groupBox2.Controls.Add(this.txtDescription);
            this.groupBox2.Controls.Add(this.txtSummary);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtCode);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Location = new System.Drawing.Point(688, 29);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(583, 827);
            this.groupBox2.TabIndex = 101;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Last Bug Version:";
            // 
            // txtAuditID
            // 
            this.txtAuditID.Location = new System.Drawing.Point(123, 19);
            this.txtAuditID.Name = "txtAuditID";
            this.txtAuditID.ReadOnly = true;
            this.txtAuditID.Size = new System.Drawing.Size(402, 26);
            this.txtAuditID.TabIndex = 114;
            // 
            // txtSourceFile
            // 
            this.txtSourceFile.Location = new System.Drawing.Point(123, 451);
            this.txtSourceFile.Name = "txtSourceFile";
            this.txtSourceFile.Size = new System.Drawing.Size(402, 26);
            this.txtSourceFile.TabIndex = 113;
            // 
            // txtMethod
            // 
            this.txtMethod.Location = new System.Drawing.Point(124, 419);
            this.txtMethod.Name = "txtMethod";
            this.txtMethod.Size = new System.Drawing.Size(402, 26);
            this.txtMethod.TabIndex = 112;
            // 
            // txtClass
            // 
            this.txtClass.Location = new System.Drawing.Point(124, 389);
            this.txtClass.Name = "txtClass";
            this.txtClass.Size = new System.Drawing.Size(402, 26);
            this.txtClass.TabIndex = 111;
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.Location = new System.Drawing.Point(123, 358);
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.ReadOnly = true;
            this.txtCreatedBy.Size = new System.Drawing.Size(402, 26);
            this.txtCreatedBy.TabIndex = 110;
            // 
            // dateResolutionDate
            // 
            this.dateResolutionDate.Location = new System.Drawing.Point(155, 328);
            this.dateResolutionDate.Name = "dateResolutionDate";
            this.dateResolutionDate.Size = new System.Drawing.Size(200, 26);
            this.dateResolutionDate.TabIndex = 109;
            // 
            // txtPriority
            // 
            this.txtPriority.Location = new System.Drawing.Point(123, 293);
            this.txtPriority.Name = "txtPriority";
            this.txtPriority.ReadOnly = true;
            this.txtPriority.Size = new System.Drawing.Size(402, 26);
            this.txtPriority.TabIndex = 108;
            // 
            // groupProgress
            // 
            this.groupProgress.Controls.Add(this.rbProgressCompleted);
            this.groupProgress.Controls.Add(this.rbProgressInProgress);
            this.groupProgress.Controls.Add(this.rbProgressNotStarted);
            this.groupProgress.Location = new System.Drawing.Point(123, 233);
            this.groupProgress.Name = "groupProgress";
            this.groupProgress.Size = new System.Drawing.Size(419, 53);
            this.groupProgress.TabIndex = 107;
            this.groupProgress.TabStop = false;
            this.groupProgress.Text = "Progress";
            // 
            // rbProgressCompleted
            // 
            this.rbProgressCompleted.AutoSize = true;
            this.rbProgressCompleted.Location = new System.Drawing.Point(291, 23);
            this.rbProgressCompleted.Name = "rbProgressCompleted";
            this.rbProgressCompleted.Size = new System.Drawing.Size(111, 24);
            this.rbProgressCompleted.TabIndex = 2;
            this.rbProgressCompleted.TabStop = true;
            this.rbProgressCompleted.Text = "Completed";
            this.rbProgressCompleted.UseVisualStyleBackColor = true;
            // 
            // rbProgressInProgress
            // 
            this.rbProgressInProgress.AutoSize = true;
            this.rbProgressInProgress.Location = new System.Drawing.Point(158, 22);
            this.rbProgressInProgress.Name = "rbProgressInProgress";
            this.rbProgressInProgress.Size = new System.Drawing.Size(115, 24);
            this.rbProgressInProgress.TabIndex = 1;
            this.rbProgressInProgress.TabStop = true;
            this.rbProgressInProgress.Text = "In Progress";
            this.rbProgressInProgress.UseVisualStyleBackColor = true;
            // 
            // rbProgressNotStarted
            // 
            this.rbProgressNotStarted.AutoSize = true;
            this.rbProgressNotStarted.Location = new System.Drawing.Point(25, 23);
            this.rbProgressNotStarted.Name = "rbProgressNotStarted";
            this.rbProgressNotStarted.Size = new System.Drawing.Size(116, 24);
            this.rbProgressNotStarted.TabIndex = 0;
            this.rbProgressNotStarted.TabStop = true;
            this.rbProgressNotStarted.Text = "Not Started";
            this.rbProgressNotStarted.UseVisualStyleBackColor = true;
            // 
            // txtProject
            // 
            this.txtProject.Location = new System.Drawing.Point(123, 201);
            this.txtProject.Name = "txtProject";
            this.txtProject.ReadOnly = true;
            this.txtProject.Size = new System.Drawing.Size(402, 26);
            this.txtProject.TabIndex = 106;
            // 
            // txtAssignedTo
            // 
            this.txtAssignedTo.Location = new System.Drawing.Point(123, 170);
            this.txtAssignedTo.Name = "txtAssignedTo";
            this.txtAssignedTo.ReadOnly = true;
            this.txtAssignedTo.Size = new System.Drawing.Size(402, 26);
            this.txtAssignedTo.TabIndex = 105;
            // 
            // txtIdentifiedBy
            // 
            this.txtIdentifiedBy.Location = new System.Drawing.Point(123, 139);
            this.txtIdentifiedBy.Name = "txtIdentifiedBy";
            this.txtIdentifiedBy.ReadOnly = true;
            this.txtIdentifiedBy.Size = new System.Drawing.Size(402, 26);
            this.txtIdentifiedBy.TabIndex = 104;
            // 
            // txtIdentifiedOn
            // 
            this.txtIdentifiedOn.Location = new System.Drawing.Point(123, 109);
            this.txtIdentifiedOn.Name = "txtIdentifiedOn";
            this.txtIdentifiedOn.ReadOnly = true;
            this.txtIdentifiedOn.Size = new System.Drawing.Size(402, 26);
            this.txtIdentifiedOn.TabIndex = 103;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(123, 78);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(402, 26);
            this.txtDescription.TabIndex = 102;
            // 
            // txtSummary
            // 
            this.txtSummary.Location = new System.Drawing.Point(123, 48);
            this.txtSummary.Name = "txtSummary";
            this.txtSummary.Size = new System.Drawing.Size(402, 26);
            this.txtSummary.TabIndex = 101;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 475);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 100;
            this.label3.Text = "Code:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 20);
            this.label5.TabIndex = 98;
            this.label5.Text = "Audit ID:";
            // 
            // txtCode
            // 
            this.txtCode.IsReadOnly = false;
            this.txtCode.Location = new System.Drawing.Point(23, 501);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(737, 367);
            this.txtCode.TabIndex = 97;
            this.txtCode.Load += new System.EventHandler(this.txtCode_Load);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 453);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 96;
            this.label6.Text = "Source File";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 390);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 95;
            this.label7.Text = "Class";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 422);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 20);
            this.label8.TabIndex = 94;
            this.label8.Text = "Method";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 296);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 20);
            this.label9.TabIndex = 93;
            this.label9.Text = "Priority";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 362);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 20);
            this.label10.TabIndex = 92;
            this.label10.Text = "Created By";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(21, 331);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 20);
            this.label11.TabIndex = 91;
            this.label11.Text = "Resolution Date";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(19, 231);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 20);
            this.label12.TabIndex = 90;
            this.label12.Text = "Progress";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 205);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 20);
            this.label13.TabIndex = 89;
            this.label13.Text = "Project";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 173);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(97, 20);
            this.label14.TabIndex = 88;
            this.label14.Text = "Assigned To";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 20);
            this.label15.TabIndex = 87;
            this.label15.Text = "Identified On";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 20);
            this.label16.TabIndex = 86;
            this.label16.Text = "Identified By";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 20);
            this.label17.TabIndex = 85;
            this.label17.Text = "Description";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 52);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 20);
            this.label18.TabIndex = 84;
            this.label18.Text = "Summary:";
            // 
            // btnUpdateBug
            // 
            this.btnUpdateBug.Location = new System.Drawing.Point(552, 414);
            this.btnUpdateBug.Name = "btnUpdateBug";
            this.btnUpdateBug.Size = new System.Drawing.Size(130, 55);
            this.btnUpdateBug.TabIndex = 102;
            this.btnUpdateBug.Text = "Update Bug";
            this.btnUpdateBug.UseVisualStyleBackColor = true;
            this.btnUpdateBug.Click += new System.EventHandler(this.btnUpdateBug_Click);
            // 
            // UpdateBug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1297, 868);
            this.Controls.Add(this.btnUpdateBug);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UpdateBug";
            this.Text = "UpdateBug";
            this.Load += new System.EventHandler(this.UpdateBug_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupProgress.ResumeLayout(false);
            this.groupProgress.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblAuditID;
        private ICSharpCode.TextEditor.TextEditorControl txtAuditCode;
        private System.Windows.Forms.Label lblSourceFile;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Label lblMethod;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.Label lblCreatedBy;
        private System.Windows.Forms.Label lblResolutionDate;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.Label lblProject;
        private System.Windows.Forms.Label lblAssignedTo;
        private System.Windows.Forms.Label lblIdentifiedOn;
        private System.Windows.Forms.Label lblIdentifiedBy;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblSummary;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private ICSharpCode.TextEditor.TextEditorControl txtCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnUpdateBug;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.TextBox txtSummary;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtProject;
        private System.Windows.Forms.TextBox txtAssignedTo;
        private System.Windows.Forms.TextBox txtIdentifiedBy;
        private System.Windows.Forms.TextBox txtIdentifiedOn;
        private System.Windows.Forms.TextBox txtPriority;
        private System.Windows.Forms.GroupBox groupProgress;
        private System.Windows.Forms.RadioButton rbProgressCompleted;
        private System.Windows.Forms.RadioButton rbProgressInProgress;
        private System.Windows.Forms.RadioButton rbProgressNotStarted;
        private System.Windows.Forms.TextBox txtSourceFile;
        private System.Windows.Forms.TextBox txtMethod;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.TextBox txtCreatedBy;
        private System.Windows.Forms.DateTimePicker dateResolutionDate;
        private System.Windows.Forms.TextBox txtAuditID;
    }
}