﻿namespace BugReporter
{
    partial class ViewMyBugs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.listBugs = new System.Windows.Forms.ListView();
            this.columnOriginalBugID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSummary = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnIdentifiedOn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnIdentifiedBy = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnPriority = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMethod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnClass = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnSourceFile = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnUpdateBug = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Location = new System.Drawing.Point(54, 409);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(263, 155);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "KEY";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.Orange;
            this.textBox3.ForeColor = System.Drawing.Color.White;
            this.textBox3.Location = new System.Drawing.Point(13, 64);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(237, 26);
            this.textBox3.TabIndex = 8;
            this.textBox3.Text = "IN PROGRESS";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Red;
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(13, 100);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(237, 26);
            this.textBox2.TabIndex = 7;
            this.textBox2.Text = "NOT STARTED";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Green;
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(13, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(237, 26);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "COMPLETED";
            // 
            // listBugs
            // 
            this.listBugs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnOriginalBugID,
            this.columnSummary,
            this.columnDescription,
            this.columnIdentifiedOn,
            this.columnIdentifiedBy,
            this.columnPriority,
            this.columnMethod,
            this.columnClass,
            this.columnSourceFile});
            this.listBugs.FullRowSelect = true;
            this.listBugs.HideSelection = false;
            this.listBugs.Location = new System.Drawing.Point(54, 33);
            this.listBugs.Name = "listBugs";
            this.listBugs.Size = new System.Drawing.Size(880, 370);
            this.listBugs.TabIndex = 7;
            this.listBugs.UseCompatibleStateImageBehavior = false;
            this.listBugs.View = System.Windows.Forms.View.Details;
            this.listBugs.SelectedIndexChanged += new System.EventHandler(this.listBugs_SelectedIndexChanged);
            // 
            // columnOriginalBugID
            // 
            this.columnOriginalBugID.Text = "Original Bug ID";
            // 
            // columnSummary
            // 
            this.columnSummary.Text = "Summary";
            this.columnSummary.Width = 85;
            // 
            // columnDescription
            // 
            this.columnDescription.Text = "Description";
            this.columnDescription.Width = 108;
            // 
            // columnIdentifiedOn
            // 
            this.columnIdentifiedOn.Text = "Identified On";
            this.columnIdentifiedOn.Width = 111;
            // 
            // columnIdentifiedBy
            // 
            this.columnIdentifiedBy.Text = "Identified By";
            this.columnIdentifiedBy.Width = 119;
            // 
            // columnPriority
            // 
            this.columnPriority.Text = "Priority";
            // 
            // columnMethod
            // 
            this.columnMethod.Text = "Method";
            // 
            // columnClass
            // 
            this.columnClass.Text = "Class";
            // 
            // columnSourceFile
            // 
            this.columnSourceFile.Text = "Source File";
            // 
            // btnUpdateBug
            // 
            this.btnUpdateBug.Location = new System.Drawing.Point(561, 461);
            this.btnUpdateBug.Name = "btnUpdateBug";
            this.btnUpdateBug.Size = new System.Drawing.Size(162, 51);
            this.btnUpdateBug.TabIndex = 9;
            this.btnUpdateBug.Text = "Update Selected Bug";
            this.btnUpdateBug.UseVisualStyleBackColor = true;
            this.btnUpdateBug.Click += new System.EventHandler(this.btnUpdateBug_Click);
            // 
            // ViewMyBugs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 597);
            this.Controls.Add(this.btnUpdateBug);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listBugs);
            this.Name = "ViewMyBugs";
            this.Text = "UpdateBug";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ViewMyBugs_FormClosed);
            this.Load += new System.EventHandler(this.UpdateBug_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListView listBugs;
        private System.Windows.Forms.ColumnHeader columnOriginalBugID;
        private System.Windows.Forms.ColumnHeader columnSummary;
        private System.Windows.Forms.ColumnHeader columnDescription;
        private System.Windows.Forms.ColumnHeader columnIdentifiedOn;
        private System.Windows.Forms.ColumnHeader columnIdentifiedBy;
        private System.Windows.Forms.ColumnHeader columnPriority;
        private System.Windows.Forms.ColumnHeader columnMethod;
        private System.Windows.Forms.ColumnHeader columnClass;
        private System.Windows.Forms.ColumnHeader columnSourceFile;
        private System.Windows.Forms.Button btnUpdateBug;
    }
}