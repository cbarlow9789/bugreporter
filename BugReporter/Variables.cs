﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugReporter
{
    class Variables
    {
        private static int _userID;
        private static String _userName;

        public static int userID
        {
            get
            {
                return _userID;
            }
            set
            {
                _userID = value;
            }
        }

        public static String userName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }
    }
}
