﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugReporter;
using System.Data.SqlClient;

namespace BugReporterTest
{
    [TestClass]
    public class NewBugTest
    {
        SqlConnection SqlConnection;

        [TestMethod]
        public void NewBugCheck()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");

            //act
            bool expected = true;
            String commandString = "INSERT INTO BUG(BUGID,SUMMARY, DESCRIPTION, IDENTIFIED_BY, IDENTIFIED_ON, ASSIGNED_TO, PROJECTID, PRIORITY, PROGRESS, RESOLUTION_DATE, CREATED_BY, MODIFIED_BY, AUDIT_ID, CODE, METHOD, CLASS, SOURCEFILE, ORIGINAL_BUGID) VALUES (1, @SUMMARY, @DESCRIPTION, @IDENTIFIED_BY, @IDENTIFIED_ON, @ASSIGNED_TO, @PROJECTID, @PRIORITY, @PROGRESS, @RESOLUTION_DATE, @CREATED_BY, @MODIFIED_BY, @AUDIT_ID, @CODE, @METHOD, @CLASS, @SOURCEFILE, @ORIGINALBUGID)";
            var NewBug = new NewBug();

            //arrange
            var result = NewBug.insertRecord("Summary", "Description", 1,DateTime.Parse("05/01/2017 22:30:44"), 1, 1, "HIGH", "NOT STARTED", DateTime.Parse("05/01/2017 22:30:44"), 1, 1, 1, "CODE", "METHOD", "CLASS", "SOURCEFILE", 1, commandString);

            //assert
            Assert.AreEqual(true, result, "return value should be {0}", expected);
        }

        [TestCleanup]
        public void CleanUp()
        {
            try
            {
                //delete created user
                SqlConnection.Open();
                String commandStringTeardown = "DELETE FROM BUG WHERE BUGID =1";
                SqlCommand cmdInsert = new SqlCommand(commandStringTeardown, SqlConnection);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {

            }
            SqlConnection.Close();
        }

    }

}