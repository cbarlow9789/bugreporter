﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugReporter;
using System.Data.SqlClient;

namespace BugReporterTest
{
    [TestClass]
    public class NewProject
    {
        SqlConnection SqlConnection;

        [TestMethod]
        public void NewProjectCheck()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");

            //act
            bool expected = true;
            String commandString = "INSERT INTO PROJECTS(PROJECTID, NAME, CREATED_BY, TARGET_END, START) VALUES (2,@name, @created_by, @target_end, @start)";
            var AddProject = new AddProject();

            //arrange
            var result = AddProject.insertRecord("Proj Name",1,DateTime.Parse("05/01/2017 22:30:44"), DateTime.Parse("05/01/2017 22:30:44"), commandString);


            //assert
            Assert.AreEqual(true, result, "return value should be {0}", expected);
        }

        [TestCleanup]
        public void CleanUp()
        {
            try
            {
                //delete created user
                SqlConnection.Open();
                String commandStringTeardown = "DELETE FROM PROJECTS WHERE PROJECTID =2";
                SqlCommand cmdInsert = new SqlCommand(commandStringTeardown, SqlConnection);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {

            }
            SqlConnection.Close();
        }

    }

}