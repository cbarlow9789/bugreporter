﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugReporter;
using System.Data.SqlClient;

namespace BugReporterTest
{
    [TestClass]
    public class LoginTest
    {
        SqlConnection SqlConnection;

        [TestMethod]
        public void LoginCheck()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");

            //act
            bool expected = true;
            String commandString = "SELECT * FROM USERS WHERE USERNAME=@USERNAME COLLATE SQL_Latin1_General_CP1_CS_AS and PASSWORD = @PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS";
            var login = new login();

            //arrange
            var result = login.loginTry("test1", "test1", commandString);


            //assert
            Assert.AreEqual(true, result, "return value should be {0}", expected);
        }

    }

}