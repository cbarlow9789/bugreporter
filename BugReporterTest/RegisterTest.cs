﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BugReporter;
using System.Data.SqlClient;

namespace BugReporterTest
{
    [TestClass]
    public class RegisterTest
    {
        SqlConnection SqlConnection;

        [TestMethod]
        public void RegisterCheck()
        {
            SqlConnection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='C:\USERS\CONOR\ONEDRIVE\DOCUMENTS\UNI WORK\SOFTWARE ENGINEERING\BUGREPORTER\BUGREPORTER\BUGREPORTER\DATABASE.MDF';Integrated Security=True;Connect Timeout=30");

            //act
            bool expected = true;
            String commandString = "INSERT INTO USERS (USERID, NAME, EMAIL, ROLE, USERNAME, PASSWORD) VALUES (2, @name, @email, @role, @username, @password)";
            var register = new register();

            //arrange
            var result = register.insertRecord("test", "test", "test", "test", "test", commandString);


            //assert
            Assert.AreEqual(true, result, "return value should be {0}", expected);
        }

        [TestCleanup]
        public void CleanUp()
        {
            try
            {
                //delete created user
                SqlConnection.Open();
                String commandStringTeardown = "DELETE FROM USERS WHERE USERID =2";
                SqlCommand cmdInsert = new SqlCommand(commandStringTeardown, SqlConnection);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {

            }
            SqlConnection.Close();
        }

    }

}